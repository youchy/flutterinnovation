import 'package:flutter/material.dart';
import 'screens/profile.dart';
import 'screens/home.dart';
import 'screens/detail.dart';
import 'widgets/searchBar.dart';
import 'package:flutter_inoovation/widgets/app.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Startup Name Generator',
      home: App()
      );
  }
}

