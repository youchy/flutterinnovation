import 'package:flutter/material.dart';
import 'package:flutter_search_bar/flutter_search_bar.dart';
import 'profileRow.dart';
import 'package:flutter_inoovation/data/data_provider.dart';
import 'package:flutter_inoovation/data/profile.dart';

class DTSearchBar extends StatefulWidget {
  @override
  DTSearchBarState createState() => new DTSearchBarState();
}

class DTSearchBarState extends State<DTSearchBar> {
  SearchBar searchBar;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  List<Profile> profiles = DataProvider().getSearchProfiles();

  DTSearchBarState() {
    searchBar = new SearchBar(
        inBar: false,
        buildDefaultAppBar: buildAppBar,
        setState: setState,
        onSubmitted: onSubmitted);
  }

  AppBar buildAppBar(BuildContext context) {
    return new AppBar(
        title: new Text('Search Bar'),
        actions: [searchBar.getSearchAction(context)]);
  }

  void onSubmitted(String value) {
    setState(() => _scaffoldKey.currentState
        .showSnackBar(new SnackBar(content: new Text('You searched $value!'))));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: searchBar.build(context),
      key: _scaffoldKey,
      body: UsersList(profiles: profiles),
    );
  }
}
