import 'package:flutter/material.dart';
import 'profileRow.dart';
import 'package:flutter_inoovation/data/post.dart';
import 'package:flutter_inoovation/data/profile.dart';

class PostList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemBuilder: (context, index) => index == 0
          ? SizedBox()
          : new PostRow(),
    );
  }

  Widget _profileRow(String name, String imageUrl) {
    return ProfileRow();
  }

  Widget _postRow(String name, String imageUrl) {
    return PostRow();
  }
}

class ProfileRow extends StatelessWidget {

  Profile profile;

  ProfileRow({this.profile});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Container(
          height: 50.0,
          width: 50.0,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.fill, image: NetworkImage(profile.avatarUrl)),
          ),
        ),
        SizedBox(width: 20.0),
        Text(profile.name, style: TextStyle(fontWeight: FontWeight.bold))
      ],
    );
  }
}

class PostRow extends StatelessWidget {

  Post post;

  PostRow({this.post});

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.fromLTRB(16.0, 25.0, 8.0, 16.0),
          child: 
            new ProfileRow(profile: post.profile)
        ),
        Flexible(
          fit: FlexFit.loose,
          child: Image.network(post.picture.url,
                              fit: BoxFit.cover)
        ),
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[Icon(Icons.favorite)]
            )
        ),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Text(post.description)
        )
      ]
    );
  }
}