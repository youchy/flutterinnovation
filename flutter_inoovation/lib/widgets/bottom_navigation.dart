import 'package:flutter/material.dart';

enum TabItem { profile, home, search }

class TabHelper {
  static TabItem item({int index}) {
    switch (index) {
      case 0:
        return TabItem.profile;
      case 1:
        return TabItem.home;
      case 2:
        return TabItem.search;
    }
    return TabItem.profile;
  }

  static String description(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.profile:
        return 'Profile';
      case TabItem.home:
        return 'Home';
      case TabItem.search:
        return 'Search';
    }
    return '';
  }
  static IconData icon(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.profile:
        return Icons.person;
      case TabItem.home:
        return Icons.home;
      case TabItem.search:
        return Icons.search;
    }
    return Icons.layers;
  }

  static MaterialColor color(TabItem tabItem) {
    return Colors.deepPurple;
  }
}

class BottomNavigation extends StatelessWidget {
  BottomNavigation({this.currentTab, this.onSelectTab});
  final TabItem currentTab;
  final ValueChanged<TabItem> onSelectTab;


  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: [
        _buildItem(tabItem: TabItem.profile),
        _buildItem(tabItem: TabItem.home),
        _buildItem(tabItem: TabItem.search),
      ],
      onTap: (index) => onSelectTab(
        TabHelper.item(index: index),
      ),
    );
  }

  BottomNavigationBarItem _buildItem({TabItem tabItem}) {

    String text = TabHelper.description(tabItem);
    IconData icon = TabHelper.icon(tabItem);
    return BottomNavigationBarItem(
      icon: Icon(
        icon,
        color: _colorTabMatching(item: tabItem),
      ),
      title: Text(
        text,
        style: TextStyle(
          color: _colorTabMatching(item: tabItem),
        ),
      ),
    );
  }

  Color _colorTabMatching({TabItem item}) {
    return currentTab == item ? TabHelper.color(item) : Colors.grey;
  }
}