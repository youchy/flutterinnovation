import 'package:flutter/material.dart';

import 'package:flutter_inoovation/widgets/bottom_navigation.dart';

import 'package:flutter_inoovation/screens/profile.dart';
import 'package:flutter_inoovation/screens/home.dart';
import 'package:flutter_inoovation/screens/detail.dart';
import 'package:flutter_inoovation/widgets/searchBar.dart';

class TabNavigatorRoutes {
  static const String profile = '/profile';
  static const String home = '/home';
  static const String search = '/search';

  static const String detail = '/detail';
}

class TabNavigator extends StatelessWidget {
  TabNavigator({this.navigatorKey, this.tabItem});
  final GlobalKey<NavigatorState> navigatorKey;
  final TabItem tabItem;

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context, {int materialIndex: 500}) {
    return {
      TabNavigatorRoutes.home: (context) => HomeScreen(),
      TabNavigatorRoutes.search: (context) => DTSearchBar(),
      TabNavigatorRoutes.detail: (context) => DetailScreen(),
      TabNavigatorRoutes.profile: (context) => ProfileScreen(),
    };
  }



  @override
  Widget build(BuildContext context) {
    var routeBuilders = _routeBuilders(context);

    return Navigator(
        key: navigatorKey,
        initialRoute: _getRoute(tabItem),
        onGenerateRoute: (routeSettings) {
          return MaterialPageRoute(
            builder: (context) => routeBuilders[routeSettings.name](context),
          );
        });
  }

  String _getRoute(TabItem tabItem) {
    switch (tabItem) {
      case TabItem.profile:
        return TabNavigatorRoutes.profile;
      case TabItem.home:
        return TabNavigatorRoutes.home;
      case TabItem.search:
        return TabNavigatorRoutes.search;
      default:
        return TabNavigatorRoutes.home;
    }
  }
}