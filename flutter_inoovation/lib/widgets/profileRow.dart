import 'package:flutter/material.dart';
import 'package:flutter_inoovation/data/profile.dart';

class UsersList extends StatelessWidget {

  UsersList({this.profiles});
  List<Profile> profiles;

  @override
  Widget build(BuildContext context) {
    return _usersList();
  }

  Widget _usersList() {
    return new ListView(
          padding: new EdgeInsets.symmetric(vertical: 8.0),
          children: _buildProfilesList()
    );
  }

    List<Widget> _buildProfilesList() {
    return profiles.map((profile) => _profileRow(profile))
                    .toList();
  }

  Widget _profileRow(Profile profile) {
    return Row(
      children: <Widget>[
        new Container(
          height: 50.0,
          width: 50.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            image: new DecorationImage(
                fit: BoxFit.fill,
                image: new NetworkImage(profile.avatarUrl)),
          ),
        ),
        new SizedBox(width: 20.0),
        new Text(profile.name,
            style: TextStyle(fontWeight: FontWeight.bold))
      ],
    );
  }
}
