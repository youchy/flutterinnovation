import 'package:flutter/material.dart';
import 'dart:async';
import 'package:flutter_inoovation/widgets/postsList.dart';
import 'package:flutter_inoovation/data/data_provider.dart';
import 'package:flutter_inoovation/data/post.dart';

class HomeScreen extends StatefulWidget {
  @override
  createState() => HomeState();
}

class HomeState extends State<HomeScreen> {

  @override
  Widget build(BuildContext context) {
    var futureBuilder = new FutureBuilder(
      future: _getData(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        switch (snapshot.connectionState) {
          case ConnectionState.none:
          case ConnectionState.waiting:
            return new Text('loading...');
          default:
            if (snapshot.hasError)
              return new Text('Error: ${snapshot.error}');
            else
              return createListView(context, snapshot);
        }
      },
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Home Page"),
      ),
      body: futureBuilder,
    );
  }

  Future<List<Post>> _getData() async {
    var dataProvider = DataProvider();
    var posts = dataProvider.getHomePosts();
    await new Future.delayed(new Duration(seconds: 0));

    return posts;
  }

  Widget createListView(BuildContext context, AsyncSnapshot snapshot) {
    List<Post> values = snapshot.data;
    return new ListView.builder(
        itemCount: values.length,
        itemBuilder: (BuildContext context, int index) {
          return new PostRow(post: values[index]);
        },
    );
  }
}
