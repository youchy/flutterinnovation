import 'package:flutter/material.dart';
import 'package:flutter_inoovation/data/data_provider.dart';
import 'package:flutter_inoovation/data/profile.dart';
import 'package:flutter_inoovation/data/post.dart';

class ProfileScreen extends StatefulWidget {
  @override
  createState() => ProfileState();
}

class ProfileState extends State<ProfileScreen> {

  var dataProvider = DataProvider();

  @override
  Widget build(BuildContext context) {
    var currentProfile = dataProvider.getCurrentprofile();

    return Scaffold (
      appBar: AppBar(
        title: Text('Profile'),
        actions: <Widget>[
          //IconButton(icon: Icon(Icons.list), onPressed: _pushSaved),
        ],
      ),
      body: new CustomScrollView(
        slivers: <Widget>[
          new SliverToBoxAdapter(
            child: Container(child: ProfileSection(profile: currentProfile),),
          ),
          new SliverPadding(
            padding: const EdgeInsets.only(bottom: 0.0),
            sliver: new SliverGrid.count(
              crossAxisSpacing: 0.0,
              crossAxisCount: 2,
              children: currentProfile.posts.map((post) => ProfileGridImage(post: post,)).toList()
            ),
          ),
        ],
      ),
    );
  }
}

class ProfilePic extends StatelessWidget {

  String url;

  ProfilePic({this.url});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 90.0,
      height: 90.0,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          image: DecorationImage(
              fit: BoxFit.fill,
              image: NetworkImage(url)
          )
      )
    );
  }
}

class ProfileName extends StatelessWidget {
  String name;

  ProfileName({this.name});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      child:
        Text(name,
          maxLines: 1,
          textAlign: TextAlign.left,
          overflow: TextOverflow.ellipsis,
          style: new TextStyle(fontSize: 22.0),
        ),
    );
  }
}

class ProfileDescription extends StatelessWidget {

  String description;

  ProfileDescription({this.description});

  @override
  Widget build(BuildContext context) {
    return Container(
      child:
        Text(description,
          textAlign: TextAlign.left,
          style: new TextStyle(fontSize: 12.0),
        ),
    );
  }
}

class ProfileSection extends StatelessWidget {
  Profile profile;

  ProfileSection({this.profile});

  @override
  Widget build(BuildContext context) {
    return Center(child: Row(
        children: <Widget>[
          Expanded(
            flex: 1,
            child: Container(
              alignment: Alignment.center,
              padding: new EdgeInsets.all(8.0),
              child: ProfilePic(url: profile.avatarUrl)
            ),
          ),
          Expanded(
            flex: 2,
            child: new Container(
              padding: new EdgeInsets.all(8.0),
              alignment: Alignment.center,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  ProfileName(name: profile.name),
                  ProfileDescription(description: profile.description)
                ],
              ),
            )
          )
        ],
      ),
    );
  }
}

class ProfileGridImage extends StatelessWidget {

  Post post;

  ProfileGridImage({this.post});

  @override
  Widget build(BuildContext context) {
    return new Container(
      width: 190.0,
      height: 190.0,
      decoration: new BoxDecoration(
          image: new DecorationImage(
              fit: BoxFit.cover,
              image: new NetworkImage(post.picture.url)
          )
      )
    );
  }
}

