import 'package:flutter/material.dart';
import '../widgets/postsList.dart';

class DetailScreen extends StatefulWidget {
  @override
  createState() => DetailState();
}

class DetailState extends State<DetailScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold (
      appBar: AppBar(
        title: Text('Detail'),
        actions: <Widget>[
          //IconButton(icon: Icon(Icons.list), onPressed: _pushSaved),
        ],
      ),
      body: new PostRow(),
    );
  }
}
