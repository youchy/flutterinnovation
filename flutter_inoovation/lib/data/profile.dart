import 'package:flutter_inoovation/data/post.dart';

class Profile {

  Profile({this.id, this.name, this.description, this.avatarUrl, this.posts});

  int id;
  String name;
  String description;
  String avatarUrl;
  List<Post> posts;

}