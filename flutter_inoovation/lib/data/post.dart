import 'package:flutter_inoovation/data/profile.dart';
import 'package:flutter_inoovation/data/picture.dart';

class Post {

  Post({this.id, this.profile, this.picture, this.description});

  int id;
  Profile profile;
  Picture picture;
  String description;

}