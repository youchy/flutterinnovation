import 'package:flutter_inoovation/data/profile.dart';
import 'package:flutter_inoovation/data/picture.dart';
import 'package:flutter_inoovation/data/post.dart';

class DataProvider {

  static Profile currentProfile;

  static List<Profile> searchProfiles;

  static List<Post> homePosts;


  DataProvider() {
    var postId = 0;

    var picture = new Picture(id: 0, url: "https://i.imgur.com/UZk1CZo.jpg");
    var profile = new Profile(id: 100, name: "Lluis", description: "This is a description", avatarUrl: "https://www.w3schools.com/w3images/avatar1.png", posts: []);
    var posts = [
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://st-listas.20minutos.es/images/2014-03/378553/list_640px.jpg?1395656733"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/main_element/public/media/image/2015/02/448052-cine-superheroes-critica-iron-man-2.jpg?itok=YGkJBDzB"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://images-na.ssl-images-amazon.com/images/I/61xd6-xh8ZL._SX425_.jpg"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://media.jungfrau.ch/image/upload/f_auto,fl_lossy,q_auto,c_crop,ar_16:8/c_scale,w_2363/v1479812835/fileadmin/Grindelwald_First_Sommer/First-Mountaincart-Panorama-Eiger-Wetterhorn-Grindelwald.jpg"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_0MvB8VobEhg45sNZ7o5NXhuIjjJ9DZsCxZTH7GwmQxMcjO6AfA"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoYmhR_LfS0BH3NDaTrsseze09pIkbsniMRroLjC7Lh49XNGt3iQ"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "http://img2.rtve.es/v/4478308?w=1600&preview=1518722400210.jpg"), description: "This is all"),
    ];
    currentProfile = new Profile(id: 0, name: "Carlos", description: "This is a description", avatarUrl: "https://www.w3schools.com/w3images/avatar2.png", posts: posts);

    searchProfiles = [
      new Profile(id: 1, name: "Lluis", description: "This is a description", avatarUrl: "https://www.w3schools.com/w3images/avatar1.png", posts: posts),
      new Profile(id: 2, name: "Delfin", description: "This is a description", avatarUrl: "https://www.w3schools.com/w3images/avatar3.png", posts: posts),
      new Profile(id: 3, name: "Joan", description: "This is a description", avatarUrl: "https://www.w3schools.com/w3images/avatar4.png", posts: posts),
    ];

    homePosts = [
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://st-listas.20minutos.es/images/2014-03/378553/list_640px.jpg?1395656733"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/main_element/public/media/image/2015/02/448052-cine-superheroes-critica-iron-man-2.jpg?itok=YGkJBDzB"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://images-na.ssl-images-amazon.com/images/I/61xd6-xh8ZL._SX425_.jpg"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://media.jungfrau.ch/image/upload/f_auto,fl_lossy,q_auto,c_crop,ar_16:8/c_scale,w_2363/v1479812835/fileadmin/Grindelwald_First_Sommer/First-Mountaincart-Panorama-Eiger-Wetterhorn-Grindelwald.jpg"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR_0MvB8VobEhg45sNZ7o5NXhuIjjJ9DZsCxZTH7GwmQxMcjO6AfA"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRoYmhR_LfS0BH3NDaTrsseze09pIkbsniMRroLjC7Lh49XNGt3iQ"), description: "This is all"),
      new Post(id: postId++, profile: profile, picture: new Picture(id: 0, url: "http://img2.rtve.es/v/4478308?w=1600&preview=1518722400210.jpg"), description: "This is all"),
    ];
  }


  Profile getCurrentprofile() {
    return currentProfile;
  }

  List<Profile> getSearchProfiles() {
    return searchProfiles;
  }

  List<Post> getHomePosts() {
    return homePosts;
  }

}
